#lang racket

(define table (make-hash))
  
(define getprop
  (case-lambda
    [(k prop) (getprop k prop #f)]
    [(k prop def)
     (let ([al (hash-ref table k (lambda () #f))])
       (if al
           (let ([v (assq prop al)])
             (if v
                 (cdr v)
                 def))
           def))]))
  
(define putprop
  (lambda (k prop nv)
    (let ([al (hash-ref table k (lambda () '()))])
      (let ([v (assq prop al)])
        (if v
            ;(set-mcdr! v nv)
            (set! v (cons (car v) nv))
            (hash-set! table k (cons (cons prop nv) al)))))))
  
(define remprop
  (lambda (k prop)
    (let ([al (hash-ref table k (lambda () #f))])
      (if al
          (hash-set!
           table k
           (let loop ((al al))
             (cond
               [(null? al) '()]
               [(eq? prop (caar al)) (cdr al)]
               [else (cons (car al) (loop (cdr al)))])))
          '()))))
  
(define (property-names k)
  (let ([al (hash-ref table k (lambda () '()))])
    (map car al)))
  
(define (property-list k)
  (hash-ref table k (lambda () '())))

(define compile-port
  (make-parameter
   (current-output-port)
   (lambda (p)
     (unless (output-port? p)
       (error 'compile-port (format "Not an output port ~s." p)))
     p)))

(define (emit . args)
  (apply fprintf (compile-port) args)
  (newline (compile-port)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define fxshift        2)
(define fxmask      #x03)
(define fxtag       #x00)
(define bool-f      #x2F)
(define bool-t      #x6F)
(define bool-bit       6)
(define boolmask    #xBF)
(define list-nil    #x3F)
(define charshift      8)
(define charmask    #x3F)
(define chartag     #x0F)
(define wordsize       4) ; bytes

(define fixnum-bits (- (* wordsize 8) fxshift))

(define fxlower (- (expt 2 (- fixnum-bits 1))))

(define fxupper (sub1 (expt 2 (- fixnum-bits 1))))

(define (fixnum? x)
  (and (integer? x) (exact? x) (<= fxlower x fxupper)))

(define (immediate? x)
  (or (fixnum? x) (boolean? x) (null? x) (char? x)))

(define (immediate-rep x)
  (cond
   [(fixnum? x) (arithmetic-shift x fxshift)]
   [(boolean? x) (if x bool-t bool-f)]
   [(null? x) list-nil]
   [(char? x) (bitwise-ior (arithmetic-shift (char->integer x) charshift) chartag)]
   [else #f]))

(define (emit-immediate x)
  (emit "  movl $~s, %eax" (immediate-rep x)))

(define-syntax define-primitive
  (syntax-rules ()
    [(_ (prim-name si arg* ...) b b* ...)
     (begin
       (putprop 'prim-name '*is-prim* #t)
       (putprop 'prim-name '*arg-count*
         (length '(arg* ...)))
       (putprop 'prim-name '*emitter*
         (lambda (si arg* ...) b b* ...)))]))

(define (primitive? x)
  (and (symbol? x) (getprop x '*is-prim*)))

(define (primitive-emitter x)
  (or (getprop x '*emitter*) (error 'primitive-emitter (format "primitive ~s has no emitter" x))))

(define (primcall? expr)
  (and (pair? expr) (primitive? (car expr))))

(define (check-primcall-args prim args)
  (= (getprop prim '*arg-count*) (length args)))

(define (emit-primcall si env expr)
  (let ([prim (car expr)] [args (cdr expr)])
    (check-primcall-args prim args)
    (apply (primitive-emitter prim) si env args)))

(define-primitive (fxadd1 si env arg)
  (emit-expr si env arg)
  (emit "  addl $~s, %eax" (immediate-rep 1)))

(define-primitive (fxsub1 si env arg)
  (emit-expr si env arg)
  (emit "  subl $~s, %eax" (immediate-rep 1)))

(define-primitive (fixnum->char si env arg)
  (emit-expr si env arg)
  (emit "  shll $~s, %eax" (- charshift fxshift))
  (emit "  orl $~s, %eax" chartag))

(define-primitive (char->fixnum si env arg)
  (emit-expr si env arg)
  (emit "  shrl $~s, %eax" (- charshift fxshift)))

(define-primitive (fixnum? si env arg)
  (emit-expr si env arg)
  (emit "  and $~s, %al" fxmask)
  (emit "  cmp $~s, %al" fxtag)
  (emit-cmp-bool))

(define (emit-cmp-bool . args)
  (emit "  ~s %al" (if (null? args) 'sete (car args)))
  (emit "  movzbl %al, %eax")
  (emit "  sal $~s, %al" bool-bit)
  (emit "  or $~s, %al" bool-f))

(define-primitive (fxzero? si env arg)
  (emit-expr si env arg)
  (emit "  cmp $~s, %al" fxtag)
  (emit-cmp-bool))

(define-primitive (null? si env arg)
  (emit-expr si env arg)
  (emit "  cmp $~s, %al" list-nil)
  (emit-cmp-bool))

(define-primitive (boolean? si env arg)
  (emit-expr si env arg)
  (emit "  and $~s, %al" boolmask)
  (emit "  cmp $~s, %al" bool-f)
  (emit-cmp-bool))

(define-primitive (char? si env arg)
  (emit-expr si env arg)
  (emit "  and $~s, %al" charmask)
  (emit "  cmp $~s, %al" chartag)
  (emit-cmp-bool))

(define-primitive (not si env arg)
  (emit-expr si env arg)
  (emit "  cmp $~s, %al" bool-f)
  (emit-cmp-bool))

(define-primitive (fxlognot si env arg)
  (emit-expr si env arg)
  (emit "  shr $~s, %eax" fxshift)
  (emit "  not %eax")
  (emit "  shl $~s, %eax" fxshift))

(define-primitive (fx+ si env arg1 arg2)
  (emit-binop si env arg1 arg2)
  (emit "  addl ~s(%rsp), %eax" si))

(define (emit-binop si env arg1 arg2)
  (emit-expr si env arg1)
  (emit-stack-save si)
  (emit-expr (next-stack-index si) env arg2))

(define (emit-stack-save si)
  (emit "  movl %eax, ~s(%rsp)" si))

(define (emit-stack-load si)
  (emit "  movl ~s(%rsp), %eax" si))

(define (next-stack-index si)
  (- si wordsize))

(define-primitive (fx- si env arg1 arg2)
  (emit-binop si env arg1 arg2)
  (emit "  subl %eax, ~s(%rsp)" si)
  (emit "  movl ~s(%rsp), %eax" si))

(define-primitive (fx* si env arg1 arg2)
  (emit-binop si env arg1 arg2)
  (emit "  shrl $~s, %eax" fxshift)
  (emit "  mull ~s(%rsp)" si))

(define-primitive (fxlogor si env arg1 arg2)
  (emit-binop si env arg1 arg2)
  (emit "  orl ~s(%rsp), %eax" si))

(define-primitive (fxlogand si env arg1 arg2)
  (emit-binop si env arg1 arg2)
  (emit "  andl ~s(%rsp), %eax" si))

(define-primitive (fx= si env arg1 arg2)
  (emit-cmp-binop 'sete si env arg1 arg2))

(define (emit-cmp-binop setx si env arg1 arg2)
  (emit-binop si env arg1 arg2)
  (emit "  cmpl %eax, ~s(%rsp)" si)
  (emit-cmp-bool setx))

(define-primitive (fx< si env arg1 arg2)
  (emit-cmp-binop 'setl si env arg1 arg2))

(define-primitive (fx<= si env arg1 arg2)
  (emit-cmp-binop 'setle si env arg1 arg2))

(define-primitive (fx> si env arg1 arg2)
  (emit-cmp-binop 'setg si env arg1 arg2))

(define-primitive (fx>= si env arg1 arg2)
  (emit-cmp-binop 'setge si env arg1 arg2))

(define unique-label
  (let ([count 0])
    (lambda ()
      (let ([L (format "L_~s" count)])
        (set! count (add1 count))
        L))))

(define unique-labels
  (let ([count 0])
    (lambda (lvars)
      (map (lambda (lvar)
             (let ([L (format "L_~s_~s" lvar count)])
               (set! count (add1 count))
               L))
           lvars))))

(define (if? expr)
  (and (list? expr) (eq? (car expr) 'if) (= 3 (length (cdr expr)))))
(define if-test cadr)
(define if-conseq caddr)
(define if-altern cadddr)

(define (emit-if si env expr)
  (let ([alt-label (unique-label)]
        [end-label (unique-label)])
    (emit-expr si env (if-test expr))
    (emit "  cmp $~s, %al" bool-f)
    (emit "  je ~a" alt-label)
    (emit-expr si env (if-conseq expr))
    (emit "  jmp ~a" end-label)
    (emit-label alt-label)
    (emit-expr si env (if-altern expr))
    (emit-label end-label)))

(define variable? symbol?)
(define (tagged-list tag expr)
  (and (list? expr) (not (null? expr)) (eq? (car expr) tag)))

(define (let? expr) (tagged-list 'let expr))
(define (let*? expr) (tagged-list 'let* expr))
(define (letrec? expr) (tagged-list 'letrec expr))
(define let-bindings cadr)
(define letrec-bindings cadr)
(define let-body caddr)
(define letrec-body caddr)
(define empty? null?)
(define first car)
(define rest cdr)
(define rhs cadr)
(define (lhs binding)
  (check-variable (car binding)))
(define (check-variable var)
  (if (variable? var)
      var
      (error 'lhs (format "~s is not a variable" var))))
(define (make-initial-env lvars labels)
  (map list lvars labels))
(define (extend-env var si new-env)
  (cons (list var si) new-env))
(define (lookup var env)
  (cond
   [(assv var env) => cadr]
   [else #f]))

(define (emit-let si env expr)
  (define (process-let bindings si new-env)
    (cond
     [(empty? bindings)
      (emit-expr si new-env (let-body expr))]
     [else
      (let ([b (first bindings)])
        (emit-expr si (if (let*? expr) new-env env) (rhs b))
        (emit-stack-save si)
        (process-let (rest bindings)
           (next-stack-index si)
           (extend-env (lhs b) si new-env)))]))
  (process-let (let-bindings expr) si env))

(define (emit-variable-ref env var)
  (cond
   [(lookup var env) => emit-stack-load]
   (else (error 'emit-variable-ref (format "undefined variable ~s" var)))))

(define (emit-expr si env expr)
  (cond
   [(immediate? expr) (emit-immediate expr)]
   [(variable? expr) (emit-variable-ref env expr)]
   [(if? expr) (emit-if si env expr)]
   [(or (let? expr) (let*? expr)) (emit-let si env expr)]
   [(primcall? expr) (emit-primcall si env expr)]
   [(app? expr env) (emit-app si env expr)]
   [else (error 'emit-expr (format "~s is not an expression" expr))]))

(define (emit-letrec expr)
  (let* ([bindings (letrec-bindings expr)]
         [lvars (map lhs bindings)]
         [lambdas (map rhs bindings)]
         [labels (unique-labels lvars)]
         [env (make-initial-env lvars labels)])
    (for-each (emit-lambda env) lambdas labels)
    (emit-scheme-entry (letrec-body expr) env)))

(define lambda-formals cadr)
(define lambda-body caddr)

(define (emit-lambda env)
  (lambda (expr label)
    (emit-function-header label)
    (let ([fmls (lambda-formals expr)]
          [body (lambda-body expr)])
      (let f ([fmls fmls] [si (- wordsize)] [env env])
        (cond
         [(empty? fmls)
          (emit-expr si env body)
          (emit "  ret")]
         [else
          (f (rest fmls)
             (- si wordsize)
             (extend-env (first fmls) si env))])))))

(define (app? expr env)
  (and (list? expr) (not (null? expr)) (lookup (call-target expr) env)))
(define call-target car)
(define call-args cdr)
(define (emit-app si env expr)
  (define (emit-arguments si args)
    (unless (empty? args)
      (emit-expr si env (first args))
      (emit-stack-save si)
      (emit-arguments (- si wordsize) (rest args))))
  (emit-arguments (- si (* 2 wordsize)) (call-args expr))
  (emit-adjust-base (+ si wordsize))
  (emit-call (lookup (call-target expr) env))
  (emit-adjust-base (- (+ si wordsize))))

(define (emit-label label)
  (emit "~a:" label))

(define (emit-function-header f)
  (emit "  .text")
  (emit "  .globl ~a" f)
  (emit "  .type ~a, @function" f)
  (emit-label f))

(define (emit-scheme-entry expr env)
  (emit-function-header "L_scheme_entry")
  (emit-expr (- wordsize) env expr)
  (emit "  ret"))

(define (emit-adjust-base si)
  (unless (= si 0) (emit "  addq $~s, %rsp" si)))

(define (emit-call label)
  (emit "  call ~a" label))

(define (emit-program program)
  (emit "##################################")
  (display program)
  (display #\newline)
  (emit "##################################")
  (emit-function-header "scheme_entry")
  ;(emit "  movq %rsp, %rcx")
  ;(emit "  movq 8(%rsp), %rsp")
  (emit-call "L_scheme_entry")
  ;(emit "  movq %rcx, %rsp")
  (emit "  ret")
  (cond 
   [(letrec? program) (emit-letrec program)]
   [else (emit-scheme-entry program (make-initial-env '() '()))]))


(emit-program `(letrec () 12))
(emit-program `(letrec () (let ([x 5]) (fx+ x x))))
(emit-program `(letrec ([f (lambda () 5)]) 7))
(emit-program `(letrec ([f (lambda () 5)]) (let ([x 12]) x)))
(emit-program `(letrec ([f (lambda () 5)]) (f)))
(emit-program `(letrec ([f (lambda () 5)]) (let ([x (f)]) x)))
(emit-program `(letrec ([f (lambda () 5)]) (fx+ (f) 6)))
(emit-program `(letrec ([f (lambda () 5)]) (fx+ 6 (f))))
(emit-program `(letrec ([f (lambda () 5)]) (fx- 20 (f))))
(emit-program `(letrec ([f (lambda () 5)]) (fx+ (f) (f))))
(emit-program `(letrec ([f (lambda () (fx+ 5 7))] [g (lambda () 13)]) (fx+ (f) (g))))
(emit-program `(letrec ([f (lambda (x) (fx+ x 12))]) (f 13)))
(emit-program `(letrec ([f (lambda (x) (fx+ x 12))]) (f (f 10))))
(emit-program `(letrec ([f (lambda (x) (fx+ x 12))]) (f (f (f 0)))))
(emit-program `(letrec ([f (lambda (x y) (fx+ x y))] [g (lambda (x) (fx+ x 12))]) (f 16 (f (g 0) (fx+ 1 (g 0))))))
(emit-program `(letrec ([f (lambda (x) (g x x))] [g (lambda (x y) (fx+ x y))]) (f 12)))
(emit-program `(letrec ([f (lambda (x) (if (fxzero? x) 1 (fx* x (f (fxsub1 x)))))]) (f 5)))
(emit-program `(letrec ([f (lambda (x acc) (if (fxzero? x) acc (f (fxsub1 x) (fx* acc x))))]) (f 5 1)))
(emit-program `(letrec ([f (lambda (x)  (if (fxzero? x) 0 (fx+ 1 (f (fxsub1 x)))))]) (f 200)))
