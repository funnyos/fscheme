(let ([f (lambda () 12)]) (f))
=> 12
(let ([f (lambda () 12)]) (f 1))
=> 
(let ([f (lambda () 12)]) (f 1 2))
=> 
(let ([f (lambda (x) (fx+ x x))]) (f))
=> 
(let ([f (lambda (x) (fx+ x x))]) (f 1))
=> 2
(let ([f (lambda (x) (fx+ x x))]) (f 1 2))
=> 
(let ([f (lambda (x y) (fx* x (fx+ y y)))]) (f))
=> 
(let ([f (lambda (x y) (fx* x (fx+ y y)))]) (f 2))
=> 
(let ([f (lambda (x y) (fx* x (fx+ y y)))]) (f 2 3))
=> 12
(let ([f (lambda (x y) (fx* x (fx+ y y)))]) (f 2 3 4))
=> 