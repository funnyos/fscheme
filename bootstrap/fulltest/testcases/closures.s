(let ((n 12)) (let ((f (lambda () n))) (f)))
=> 12
(let ((n 12)) (let ((f (lambda (m) (fx+ n m)))) (f 100)))
=> 112
(let ((f (lambda (f n m) (if (fxzero? n) m (f (fxsub1 n) (fx* n m)))))) (let ((g (lambda (g n m) (f (lambda (n m) (g g n m)) n m)))) (g g 5 1)))
=> 120
(let ((f (lambda (f n) (if (fxzero? n) 1 (fx* n (f (fxsub1 n))))))) (let ((g (lambda (g n) (f (lambda (n) (g g n)) n)))) (g g 5)))
=> 120