(let ([f (lambda (x) x)]) (f 12))
=> 12
(let ([f (lambda (x y) (fx+ x y))]) (f 12 13))
=> 25
(let ([f (lambda (x) (let ([g (lambda (x y) (fx+ x y))]) (g x 100)))]) (f 1000))
=> 1100
(let ([f (lambda (g) (g 2 13))]) (f (lambda (n m) (fx* n m))))
=> 26
(let ([f (lambda (g) (fx+ (g 10) (g 100)))]) (f (lambda (x) (fx* x x))))
=> 10100
(let ([f (lambda (f n m) (if (fxzero? n) m (f f (fxsub1 n) (fx* n m))))]) (f f 5 1))
=> 120
(let ([f (lambda (f n) (if (fxzero? n) 1 (fx* n (f f (fxsub1 n)))))]) (f f 5))
=> 120