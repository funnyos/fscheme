(fxquotient 16 4)
=> 4
(fxquotient 5 2)
=> 2
(fxquotient -45 7)
=> -6
(fxquotient 10 -3)
=> -3
(fxquotient -17 -9)
=> 1
(fxremainder 16 4)
=> 0
(fxremainder 5 2)
=> 1
(fxremainder -45 7)
=> -3
(fxremainder 10 -3)
=> 1
(fxremainder -17 -9)
=> -8