(letrec ((f (lambda (i lst) (if (fx= i 0) lst (f (fxsub1 i) (cons i lst)))))) (f 10 ()))
=> (1 2 3 4 5 6 7 8 9 10)