(fxsub1 0)
=> -1
(fxsub1 -1)
=> -2
(fxsub1 1)
=> 0
(fxsub1 -100)
=> -101
(fxsub1 1000)
=> 999
(fxsub1 536870911)
=> 536870910
(fxsub1 -536870911)
=> -536870912
(fxsub1 (fxsub1 0))
=> -2
(fxsub1 (fxsub1 (fxsub1 (fxsub1 (fxsub1 (fxsub1 12))))))
=> 6
(fxsub1 (fxadd1 0))
=> 0