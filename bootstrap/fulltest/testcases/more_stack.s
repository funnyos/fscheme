(letrec ([f (lambda (n) (if (fxzero? n) 0 (fx+ 1 (f (fxsub1 n)))))]) (f 500))
=> 500