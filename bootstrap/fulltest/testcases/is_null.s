(null? ())
=> #t
(null? #f)
=> #f
(null? #t)
=> #f
(null? (null? ()))
=> #f
(null? #\a)
=> #f
(null? 0)
=> #f
(null? -10)
=> #f
(null? 10)
=> #f