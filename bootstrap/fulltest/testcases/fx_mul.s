(fx* 2 3)
=> 6
(fx* 2 -3)
=> -6
(fx* -2 3)
=> -6
(fx* -2 -3)
=> 6
(fx* 536870911 1)
=> 536870911
(fx* 536870911 -1)
=> -536870911
(fx* -536870912 1)
=> -536870912
(fx* -536870911 -1)
=> 536870911
(fx* 2 (fx* 3 4))
=> 24
(fx* (fx* 2 3) 4)
=> 24
(fx* (fx* (fx* (fx* (fx* 2 3) 4) 5) 6) 7)
=> 5040
(fx* 2 (fx* 3 (fx* 4 (fx* 5 (fx* 6 7)))))
=> 5040