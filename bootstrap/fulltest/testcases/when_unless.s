(let ([x (cons 1 2)]) (when (pair? x) (set-car! x (fx+ (car x) (cdr x)))) x)
=> (3 . 2)
(let ([x (cons 1 2)]) (when (pair? x) (set-car! x (fx+ (car x) (cdr x))) (set-car! x (fx+ (car x) (cdr x)))) x)
=> (5 . 2)
(let ([x (cons 1 2)]) (unless (fixnum? x) (set-car! x (fx+ (car x) (cdr x)))) x)
=> (3 . 2)
(let ([x (cons 1 2)]) (unless (fixnum? x) (set-car! x (fx+ (car x) (cdr x))) (set-car! x (fx+ (car x) (cdr x)))) x)
=> (5 . 2)
(let ([let 12]) (when let let let let let))
=> 12
(let ([let #f]) (unless let let let let let))
=> #f