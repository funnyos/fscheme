(let () (define x 3) (define y 4) (fx+ x y))
=> 7
(let () (define x 3) (set! x 4) (define y x) (fx+ x y))
=> 8