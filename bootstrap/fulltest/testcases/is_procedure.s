(procedure? (lambda (x) x))
=> #t
(let ([f (lambda (x) x)]) (procedure? f))
=> #t
(procedure? (make-vector 0))
=> #f
(procedure? (make-string 0))
=> #f
(procedure? (cons 1 2))
=> #f
(procedure? #\S)
=> #f
(procedure? ())
=> #f
(procedure? #t)
=> #f
(procedure? #f)
=> #f
(string? (lambda (x) x))
=> #f
(vector? (lambda (x) x))
=> #f
(boolean? (lambda (x) x))
=> #f
(null? (lambda (x) x))
=> #f
(not (lambda (x) x))
=> #f