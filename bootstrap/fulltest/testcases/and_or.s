(and)
=> #t
(and 5)
=> 5
(and #f)
=> #f
(and 5 6)
=> 6
(and #f ((lambda (x) (x x)) (lambda (x) (x x))))
=> #f
(or)
=> #f
(or #t)
=> #t
(or 5)
=> 5
(or 1 2 3)
=> 1
(or (cons 1 2) ((lambda (x) (x x)) (lambda (x) (x x))))
=> (1 . 2)
(let ((if 12)) (or if 17))
=> 12
(let ((if 12)) (and if 17))
=> 17
(let ((let 8)) (or let 18))
=> 8
(let ((let 8)) (and let 18))
=> 18
(let ((t 1)) (and (begin (set! t (fxadd1 t)) t) t))
=> 2
(let ((t 1)) (or (begin (set! t (fxadd1 t)) t) t))
=> 2