(let ([f (lambda (a b c) (string a b c))]) (f #\a #\b #\c))
=> "abc"
(let ([f (lambda (a b c) (string a b c))]) (f #\a 12 #\c))
=> 
(let ([f string]) (f #\a #\b #\c))
=> "abc"
(let ([f string]) (f #\a #\b 'x))
=> 
(string #\a #\b #\c)
=> "abc"
(string #\a #\b #t)
=> 