(begin 12)
=> 12
(begin 13 122)
=> 122
(begin 123 2343 #t)
=> #t
(let ((t (begin 12 (cons 1 2)))) (begin t t))
=> (1 . 2)
(let ((t (begin 13 (cons 1 2)))) (cons 1 t) t)
=> (1 . 2)
(let ((t (cons 1 2))) (if (pair? t) (begin t) 12))
=> (1 . 2)