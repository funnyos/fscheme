(fixnum->char 65)
=> #\A
(fixnum->char 97)
=> #\a
(fixnum->char 122)
=> #\z
(fixnum->char 90)
=> #\Z
(fixnum->char 48)
=> #\0
(fixnum->char 57)
=> #\9
(char->fixnum #\A)
=> 65
(char->fixnum #\a)
=> 97
(char->fixnum #\z)
=> 122
(char->fixnum #\Z)
=> 90
(char->fixnum #\0)
=> 48
(char->fixnum #\9)
=> 57
(char->fixnum (fixnum->char 12))
=> 12
(fixnum->char (char->fixnum #\x))
=> #\x