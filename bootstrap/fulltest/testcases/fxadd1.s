(fxadd1 0)
=> 1
(fxadd1 -1)
=> 0
(fxadd1 1)
=> 2
(fxadd1 -100)
=> -99
(fxadd1 1000)
=> 1001
(fxadd1 536870910)
=> 536870911
(fxadd1 -536870912)
=> -536870911
(fxadd1 (fxadd1 0))
=> 2
(fxadd1 (fxadd1 (fxadd1 (fxadd1 (fxadd1 (fxadd1 12))))))
=> 18