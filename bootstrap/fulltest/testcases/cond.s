(cond (1 2) (else 3))
=> 2
(cond (1) (else 13))
=> 1
(cond (#f #t) (#t #f))
=> #f
(cond (else 17))
=> 17
(cond (#f) (#f 12) (12 13))
=> 13
(cond ((cons 1 2) => (lambda (x) (cdr x))))
=> 2
(let ((else #t)) (cond (else 1287)))
=> 1287
(let ((else 17)) (cond (else)))
=> 17
(let ((else 17)) (cond (else => (lambda (x) x))))
=> 17
(let ((else #f)) (cond (else ((lambda (x) (x x)) (lambda (x) (x x))))) else)
=> #f
(let ((=> 12)) (cond (12 => 14) (else 17)))
=> 14
(let ((=> 12)) (cond (=>)))
=> 12
(let ((=> 12)) (cond (=> =>)))
=> 12
(let ((=> 12)) (cond (=> => =>)))
=> 12
(let ((let 12)) (cond (let => (lambda (x) (fx+ let x))) (else 14)))
=> 24