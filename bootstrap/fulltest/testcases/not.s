(not #t)
=> #f
(not #f)
=> #t
(not 15)
=> #f
(not ())
=> #f
(not #\A)
=> #f
(not (not #t))
=> #t
(not (not #f))
=> #f
(not (not 15))
=> #t
(not (fixnum? 15))
=> #f
(not (fixnum? #f))
=> #t