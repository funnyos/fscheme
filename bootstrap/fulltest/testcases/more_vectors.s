(letrec ((f (lambda (v i) (if (fx>= i 0) (begin (vector-set! v i i) (f v (fxsub1 i))) v)))) (let ((v (make-vector 100))) (vector-length (f v (fxsub1 100)))))
=> 100