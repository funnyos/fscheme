(let ((t 1)) (and (begin (set! t (fxadd1 t)) t) t))
=> 2
(let ((f (if (boolean? (lambda () 12))(lambda () 13) (lambda () 14)))) (f))
=> 14
(let ([f 12])(let ([g (lambda () f)])(g)))
=> 12
(fx< 1 2)
=> #t
(let ([f (lambda (x y) (fx< x y))]) (f 10 10))
=> #f
(fx< 10 10)
=> #f
(fx< 10 2)
=> #f
(fx<= 1 2)
=> #t
(fx<= 10 10)
=> #t
(fx<= 10 2)
=> #f
(let ([x 12]) (string-set! x 0 #\a))
=> 
(let ([x (string #\a #\b #\c)] [y 12]) (string-set! x 0 y))
=> 
(let ([x (string #\a #\b #\c)] [y 12]) (string-set! x 8 y))
=> 
(let ([x (string #\a #\b #\c)] [y #\a]) (string-set! x 8 y))
=> 
(let ([x (string #\a #\b #\c)]) (string-set! x 8 #\a))
=> 
(let ([x (string #\a #\b #\c)] [y #\a]) (string-set! x -1 y))
=> 
(let ([s (string #\a #\b #\c)] [i 1] [c #\X]) (string-set! s i c) s)
=> "aXc"
(let ([s (string #\a #\b #\c)] [i 1]) (string-set! s i #\X) s)
=> "aXc"
(let ([s (string #\a #\b #\c)] [i 1] [c 'X]) (string-set! s i c) s)
=> 
(let ([s (string #\a #\b #\c)] [i 1] [c #\X]) (string-set! s 1 c) s)
=> "aXc"
(let ([s (string #\a #\b #\c)] [i 1]) (string-set! s 1 #\X) s)
=> "aXc"
(let ([s (string #\a #\b #\c)] [i 1] [c 'X]) (string-set! s 1 c) s)
=> 
(let ([s (string #\a #\b #\c)] [i 3] [c #\X]) (string-set! s i c) s)
=> 
(let ([s (string #\a #\b #\c)] [i 3]) (string-set! s i #\X) s)
=> 
(let ([s (string #\a #\b #\c)] [i 3] [c 'X]) (string-set! s i c) s)
=> 
(let ([s (string #\a #\b #\c)] [i -10] [c #\X]) (string-set! s i c) s)
=> 
(let ([s (string #\a #\b #\c)] [i -11]) (string-set! s i #\X) s)
=> 
(let ([s (string #\a #\b #\c)] [i -1] [c 'X]) (string-set! s i c) s)
=> 
(let ([s (string #\a #\b #\c)] [i 'foo] [c #\X]) (string-set! s i c) s)
=> 
(let ([s (string #\a #\b #\c)] [i 'foo]) (string-set! s i #\X) s)
=> 
(let ([s (string #\a #\b #\c)] [i 'foo] [c 'X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 1] [c #\X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 1]) (string-set! s i #\X) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 1] [c 'X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 1] [c #\X]) (string-set! s 1 c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 1]) (string-set! s 1 #\X) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 1] [c 'X]) (string-set! s 1 c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 3] [c #\X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 3]) (string-set! s i #\X) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 3] [c 'X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i -10] [c #\X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i -11]) (string-set! s i #\X) s)
=> 
(let ([s '(string #\a #\b #\c)] [i -1] [c 'X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 'foo] [c #\X]) (string-set! s i c) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 'foo]) (string-set! s i #\X) s)
=> 
(let ([s '(string #\a #\b #\c)] [i 'foo] [c 'X]) (string-set! s i c) s)
=> 