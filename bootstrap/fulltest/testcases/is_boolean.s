(boolean? #t)
=> #t
(boolean? #f)
=> #t
(boolean? 0)
=> #f
(boolean? 1)
=> #f
(boolean? -1)
=> #f
(boolean? ())
=> #f
(boolean? #\a)
=> #f
(boolean? (boolean? 0))
=> #t
(boolean? (fixnum? (boolean? 0)))
=> #t