(fx+ -536870911 -1)
=> -536870912
(begin (write (quote (1 2 3))) (exit))
=> (1 2 3)
(begin (write (quote "Hello World!")) (exit))
=> "Hello World!"