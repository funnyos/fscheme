(let ([f (lambda () 12)]) (f))
=> 12
(let ([f (lambda () (fx+ 12 13))]) (f))
=> 25
(let ([f (lambda () 13)]) (fx+ (f) (f)))
=> 26
(let ([f (lambda () (let ([g (lambda () (fx+ 2 3))]) (fx* (g) (g))))]) (fx+ (f) (f)))
=> 50
(let ([f (lambda () (let ([f (lambda () (fx+ 2 3))]) (fx* (f) (f))))]) (fx+ (f) (f)))
=> 50
(let ([f (if (boolean? (lambda () 12)) (lambda () 13) (lambda () 14))]) (f))
=> 14