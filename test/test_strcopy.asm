################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
heap:          .space  0x10000
intro:         .asciiz   "Welcome to fscheme!\n"

################################################################################
.text

.globl main

################################################################################
# main()
# entry point
################################################################################
main:
    la   $a0, intro
    la   $a1, heap
    li   $a2, 21
    jal  string_copy
    move $a0, $v0
    li   $v0, 4  # print_string
    syscall
    j    exit
################################################################################
# description:
# (char* <v0>, char* <v1>) string_copy(char* <a0>, char* <a1>, int <a2>)
# copy (char* <a0>) to (char* <a1>) with length (int <a2>)
# args:
#   a0: source
#   a1: target
#   a2: length
# returns:
#   v0: source address
#   v1: target address 
################################################################################
string_copy:
    addi $sp, $sp, -20
    sw   $ra, 16($sp)
    sw   $t3, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    move $t2, $a0
    move $t3, $a1
    move $t1, $a1
string_copy.loop:
    lbu  $t0, 0($a0)
    addi $a0, $a0, 1
    sb   $t0, 0($t1)
    addi $t1, $t1, 1
    addi $a2, $a2, -1
    bnez $a2, string_copy.loop
    move $v0, $t2
    move $v1, $t3
    
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $t3, 12($sp)
    lw   $ra, 16($sp)
    addi $sp, $sp, 20
    jr   $ra
################################################################################
# exit
################################################################################
exit:
    li   $v0, 17  # exit
    syscall
