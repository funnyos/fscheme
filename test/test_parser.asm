################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.eqv    word_size      4
.eqv    pair_size      12
.eqv    atom_size      12
.eqv    operator_size  64
.eqv    literal_size   256
.eqv    type_nil         0x1000
.eqv    type_token       0x2000
.eqv    type_pair        0x0100
.eqv    type_string      0x0000
.eqv    type_symbol      0x0001
.eqv    type_boolean     0x0002
.eqv    type_integer     0x0003
.eqv    type_fraction    0x0004
.eqv    type_decimal     0x0005
.eqv    type_character   0x0006
.eqv    type_vector      0x0007
.data
zero:             .double 0.0
ten:              .double 10.0
one:              .double 1.0
minus_one:        .double -1.0
nil_string:       .asciiz "()"
lparen_string:    .asciiz "( "
rparen_string:    .asciiz " )"
dot_string:       .asciiz " . "
t_string:         .asciiz "#t"
f_string:         .asciiz "#f"
sexp:             .asciiz "( a ( b \"1 2 3\") \"123\"@03 sdf334 )"
err_msg:          .asciiz "Error!"
################################################################################
.text

.globl main

################################################################################
# main()
# entry point
################################################################################
main:
    li   $a0, pair_size
    li   $v0, 9
    syscall
    li   $t1, type_nil
    sw   $t1, 0($v0)
    sw   $zero, 4($v0)
    sw   $zero, 8($v0)
    move $s6, $v0
    la   $s0, sexp
    jal  read_list
    lbu  $t1, 0($s0)
    bne  $t1, $zero, main.error
    move $a0, $v0
    jal  print_sexp
    j    exit
    
main.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
################################################################################
# description:
# char* <s0> eat_whitespace(char* <s0>)
# advance the buffer pointer until it reaches the next non-whitespace char
# args:
#   s0: string pointer
# returns:
#   s0: pointer point to the next non-whitespace char
################################################################################
eat_whitespace:
    addi $sp, $sp, -16
    sw   $ra, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    move $t0, $s0
    move $t1, $zero    # make sure that the upper half of $t1 is cleared
eat_whitespace.loop:
    lbu  $t1, 0($t0)   # get the character to be tested
    addi $t0, $t0, 1   # advance the buffer pointer
    beq  $t1, $zero, eat_whitespace.exit
    li   $t2, ' '
    beq  $t1, $t2, eat_whitespace.loop
    li   $t2, '\t'
    beq  $t1, $t2, eat_whitespace.loop
    li   $t2, '\r'
    beq  $t1, $t2, eat_whitespace.loop
    li   $t2, '\n'
    beq  $t1, $t2, eat_whitespace.loop
    li   $t2, ';'
    beq  $t1, $t2, eat_whitespace.eat_comments
    j    eat_whitespace.exit
eat_whitespace.eat_comments:
    move $s0, $t0
    jal  eat_comments
    move $t0, $s0
    j    eat_whitespace.loop
eat_whitespace.exit:
    addi $s0, $t0, -1  # set the return value to the current pointer
    
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $ra, 12($sp)
    addi $sp, $sp, 16
    jr $ra

################################################################################
# description:
# char* <s0> eat_comments(char* <s0>)
# advance the buffer pointer until it reaches the next non-whitespace char
# args:
#   s0: string pointer
# returns:
#   s0: pointer point to the next non-whitespace char
################################################################################
eat_comments:
    addi $sp, $sp, -16
    sw   $ra, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    move $t0, $s0
    move $t1, $zero
eat_comments.loop:
    lbu  $t1, 0($t0)   # get the character to be tested
    addi $t0, $t0, 1   # advance the buffer pointer
    beq  $t1, $zero, eat_comments.exit
    li   $t2, '\n'
    beq  $t1, $t2, eat_comments.exit
    j    eat_comments.loop
eat_comments.exit:
    addi $s0, $t0, -1  # set the return value to the current pointer

    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $ra, 12($sp)
    addi $sp, $sp, 16
    jr $ra
################################################################################
# description:
# (void* <s5>, int <s4>, void* <v0>) token(char* <s0>)
# read token (char* <s0>) and store to address (void* <v0>) with type (int <v1>)
# args:
#   s0: token pointer
# returns:
#   s5: result address
#   s4: token type
################################################################################
token:
    addi $sp, $sp, -20
    sw   $ra, 16($sp)
    sw   $s2, 12($sp)
    sw   $s1, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    jal  eat_whitespace
    lbu  $t0, 0($s0)
    move $t1, $zero
    bne  $t0, $t1, token.lparen
    li   $s4, type_token
    move $s5, $zero
    j    token.exit
token.lparen:
    lbu  $t0, 0($s0)
    li   $t1, '('
    bne  $t0, $t1, token.rparen
    addi $s0, $s0, 1
    li   $s4, type_token
    li   $s5, '('
    j    token.exit
token.rparen:
    lbu  $t0, 0($s0)
    li   $t1, ')'
    bne  $t0, $t1, token.dot
    addi $s0, $s0, 1
    li   $s4, type_token
    li   $s5, ')'
    j    token.exit
token.dot:
    lbu  $t0, 0($s0)
    li   $t1, '.'
    bne  $t0, $t1, token.dquote
    addi $s0, $s0, 1
    li   $s4, type_token
    li   $s5, '.'
    j    token.exit
token.dquote:
    lbu  $t0, 0($s0)
    li   $t1, '"'
    bne  $t0, $t1, token.other
    jal  read_literal
    j    token.exit
token.other:
    jal  read_operator

token.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $s1, 8($sp)
    lw   $s2, 12($sp)
    lw   $ra, 16($sp)
    addi $sp, $sp, 20
    jr $ra

################################################################################
# description:
# (char* <v0>) read_literal(char* <s0>)
# read literal (char* <s0>) and store literal address to (char* <v0>)
# args:
#   s0: literal pointer
# returns:
#   v0: literal address
################################################################################
read_literal:
    addi $sp, $sp, -20
    sw   $ra, 16($sp)
    sw   $s3, 12($sp)
    sw   $s2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    jal  read_string
    move $a0, $v0
    move $a1, $zero
    lbu  $t1, 0($s0)
    beq  $t1, $zero, read_literal.exit
    jal  is_delimiter
    bne  $v0, $zero, read_literal.delimiter
    li   $t0, '@'
    bne  $t0, $t1, read_literal.error
    addi $s0, $s0, 1
    jal  read_enum
    move $a1, $v0
    jal  get_literal
    j    read_literal.exit
read_literal.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
    
read_literal.delimiter:
    move $s2, $a0
    # $s3 contains string length
    li   $a0, atom_size
    li   $v0, 9
    syscall
    sw   $zero, 0($v0)
    sw   $s2, 4($v0)
    sw   $s3, 8($v0)

read_literal.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $s2, 8($sp)
    lw   $s3, 12($sp)
    lw   $ra, 16($sp)
    addi $sp, $sp, 20
    jr $ra


################################################################################
# description:
# (char* <v0>) read_string(char* <s0>)
# read string (char* <s0>) and store string address to (char* <v0>)
# args:
#   s0: string pointer
# returns:
#   v0: string address
################################################################################
read_string:
    addi $sp, $sp, -20
    sw   $ra, 16($sp)
    sw   $t3, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    addi $s0, $s0, 1
    li   $t1, literal_size
    move $a0, $t1
    li   $v0, 9
    syscall
    move $t2, $v0
    addi $t1, $t1, -1
    
read_string.loop:
    lbu  $t3, 0($s0)
    li   $t0, '"'
    beq  $t0, $t3, read_string.done
    li   $t0, '\\'
    bne  $t0, $t3, read_string.eos
    addi $s0, $s0, 1
    li   $t0, 'n'
    bne  $t0, $t3, read_string.escape_tab
    li   $t0, '\n'
    sb   $t0, 0($t2)
    addi $t2, $t2, 1
read_string.escape_tab:
    li   $t0, 't'
    bne  $t0, $t3, read_string.escape_backslash
    li   $t0, '\t'
    sb   $t0, 0($t2)
    addi $t2, $t2, 1
read_string.escape_backslash:
    li   $t0, '\\'
    bne  $t0, $t3, read_string.escape_dquote
    # li   $t0, '\\'
    sb   $t0, 0($t2)
    addi $t2, $t2, 1
read_string.escape_dquote:
    li   $t0, '"'
    bne  $t0, $t3, read_string.eos
    # li   $t0, '"'
    sb   $t0, 0($t2)
    addi $t2, $t2, 1
read_string.eos:
    beq  $s0, $zero, read_string.error
    sb   $t3, 0($t2)
    addi $s0, $s0, 1
    addi $t2, $t2, 1
    addi $t1, $t1, -1
    bnez $t1, read_string.loop
read_string.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
read_string.done:
    sb   $zero, 0($t2)
    addi $s0, $s0, 1
    move $s1, $zero
    move $s2, $v0
    sub  $s3, $t2, $v1
    
read_string.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $t3, 12($sp)
    lw   $ra, 16($sp)
    addi $sp, $sp, 20
    jr $ra

################################################################################
# description:
# (int <v0>) read_enum(char* <s0>)
# read enum (char* <s0>) and return enum value (int <v0>)
# args:
#   s0: enum string pointer
# returns:
#   v0: enum value
################################################################################
read_enum:
    addi $sp, $sp, -12
    sw   $ra, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    move $v0, $zero
read_enum.loop:
    lbu  $t0, 0($s0)
    jal  is_delimiter
    bne  $v0, $zero, read_enum.done
    li   $t1, '0'
    bltu $t0, $t1, read_enum.error
    li   $t1, '9'
    bltu $t1, $t0, read_enum.error
    subi $t0, $t0, '0'
    sll  $v0, $v0, 4
    add  $v0, $v0, $t0
    addi $s0, $s0, 1
    j    read_enum.loop
read_enum.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
    
read_enum.done:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $ra, 8($sp)
    addi $sp, $sp, 12
    jr $ra

################################################################################
# description:
# (void* <v0>) get_literal(char* <a0>, int <a1>)
# get literal (char* <a0>) with type (int <a1>)
# args:
#   a0: literal string pointer
#   a1: literal type
# returns:
#   v0: literal value
# s-expression storage:
# 1. nil
#           0x1000   |    nil    |    nil
# 2. pair
#           0x0100   |    car    |    cdr
# 3. atom
# 3.1 string
#           0x0000   | str addr  | str length
# 3.2 symbol
#           0x0001   | sym addr  | sym length
# 3.3 boolean
#           0x0002   | bool val  | 
# 3.4 integer
#           0x0003   | int  val  | 1
# 3.5 fraction
#           0x0004   | numer val | denom val 
# 3.6 decimal
#           0x0005   | double val| double val 
# 3.7 character
#           0x0006   | int  val  | 
# 3.8 vector
#           0x0007   | vec addr  | vec length
################################################################################
get_literal:
    addi $sp, $sp, -28
    sw   $ra, 24($sp)
    sw   $t2, 20($sp)
    sw   $t1, 16($sp)
    sw   $t0, 12($sp)
    sw   $s3, 8($sp)
    sw   $s2, 4($sp)
    sw   $s1, 0($sp)
        
    bne  $a1, $zero, get_literal.get_symbol
    move $s2, $a0
    # $s3 contains string length
get_literal.get_symbol:
    li   $t1, type_symbol
    bne  $a1, $t1, get_literal.get_boolean
    move $s2, $a0
    # $s3 contains string length
    j    get_literal.exit
get_literal.get_boolean:
    li   $t1, type_boolean
    bne  $a1, $t1, get_literal.get_integer
    lbu  $t2, 0($a0)
    li   $t1, 't'
    bne  $t1, $t2, get_literal.get_boolean.false
    li   $s2, 1
    j    get_literal.get_boolean.done
get_literal.get_boolean.false:
    move $s2, $zero
get_literal.get_boolean.done:
    j    get_literal.exit
get_literal.get_integer:
    li   $t1, type_integer
    bne  $a1, $t1, get_literal.get_fraction
    jal  str_to_int
    move $s2, $v0
    li   $s3, 1
    j    get_literal.exit
get_literal.get_fraction:
    li   $t1, type_fraction
    bne  $a1, $t1, get_literal.get_decimal
    jal  str_to_fraction
    move $s2, $v0
    move $s3, $v1
    j    get_literal.exit
get_literal.get_decimal:
    li   $t1, type_decimal
    bne  $a1, $t1, get_literal.get_character
    jal  str_to_float
    mfc1.d  $s2, $f2
    j    get_literal.exit
get_literal.get_character:
    li   $t1, type_character
    bne  $a1, $t1, get_literal.error
    lbu  $s2, 0($a0)
    j    get_literal.exit
get_literal.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
    
get_literal.exit:
    move $s1, $a1
    
    li   $a0, atom_size
    li   $v0, 9
    syscall
    sw   $s1, 0($v0)
    sw   $s2, 4($v0)
    sw   $s3, 8($v0)
    
    lw   $s1, 0($sp)
    lw   $s2, 4($sp)
    lw   $s3, 8($sp)
    lw   $t0, 12($sp)
    lw   $t1, 16($sp)
    lw   $t2, 20($sp)
    lw   $ra, 24($sp)
    addi $sp, $sp, 28
    jr   $ra

################################################################################
# str_to_int()
# 
################################################################################
str_to_int:
    addi $sp, $sp, -16
    sw   $ra, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    move $v0, $zero
    lbu  $t0, 0($a0)
    li   $t1, '-'
    bne  $t0, $t1, str_to_int.positive
    li   $t2, 1
    addi $a0, $a0, 1
    j    str_to_int.calc
str_to_int.positive:
    move $t2, $zero
    li   $t1, '+'
    bne  $t0, $t1, str_to_int.calc
    addi $a0, $a0, 1
str_to_int.calc:
    lbu  $t0, 0($a0)
    beq  $t0, $zero, str_to_int.done
    li   $t1, '0'
    bltu $t0, $t1, str_to_int.error
    li   $t1, '9'
    bltu $t1, $t0, str_to_int.error
    subi $t0, $t0, '0'
    mul  $v0, $v0, 10
    add  $v0, $v0, $t0
    addi $a0, $a0, 1
    j    str_to_int.calc
str_to_int.done:
    beq  $t2, $zero, str_to_int.exit
    mul  $v0, $v0, -1
    j    str_to_int.exit
str_to_int.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
    
str_to_int.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $ra, 12($sp)
    addi $sp, $sp, 16
    jr $ra

################################################################################
# str_to_fraction()
# 
################################################################################
str_to_fraction:
    addi $sp, $sp, -20
    sw   $ra, 16($sp)
    sw   $t3, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    move $v0, $zero
    move $v1, $zero
    move $t3, $zero
    
    lbu  $t0, 0($a0)
    li   $t1, '-'
    bne  $t0, $t1, str_to_fraction.positive
    li   $t2, 1
    addi $a0, $a0, 1
    j    str_to_fraction.calc
str_to_fraction.positive:
    move $t2, $zero
    li   $t1, '+'
    bne  $t0, $t1, str_to_fraction.calc
    addi $a0, $a0, 1
    
str_to_fraction.calc:
    
    lbu  $t0, 0($a0)
    beq  $t0, $zero, str_to_fraction.done
    li   $t1, '/'
    bne  $t0, $t1, str_to_fraction.not_slash
    li   $t3, 1
    addi $a0, $a0, 1
    j    str_to_fraction.calc
str_to_fraction.not_slash:
    li   $t1, '0'
    bltu $t0, $t1, str_to_fraction.error
    li   $t1, '9'
    bltu $t1, $t0, str_to_fraction.error
    subi $t0, $t0, '0'
    beq  $t3, $zero, str_to_fraction.other
    mul  $v1, $v1, 10
    add  $v1, $v1, $t0
    j    str_to_fraction.inc
str_to_fraction.other:
    mul  $v0, $v0, 10
    add  $v0, $v0, $t0
str_to_fraction.inc:
    addi $a0, $a0, 1
    j    str_to_fraction.calc
str_to_fraction.done:
    beq  $t3, $zero, str_to_fraction.error
    beq  $t2, $zero, str_to_fraction.exit
    mul  $v0, $v0, -1
    j    str_to_fraction.exit
str_to_fraction.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    li   $v0, 17  # exit
    syscall
    
str_to_fraction.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $t3, 12($sp)
    lw   $ra, 16($sp)
    addi $sp, $sp, 20
    jr $ra

################################################################################
# str_to_float()
# 
################################################################################
str_to_float:
    addi $sp, $sp, -16
    sw   $ra, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    l.d  $f8, ten
    l.d  $f2, zero
    l.d  $f4, one
    move $t2, $zero
    
    lbu  $t0, 0($a0)
    li   $t1, '-'
    bne  $t0, $t1, str_to_float.positive
    l.d  $f4, minus_one
    addi $a0, $a0, 1
    j    str_to_float.calc
str_to_float.positive:
    lbu  $t0, 0($a0)
    li   $t1, '+'
    bne  $t0, $t1, str_to_float.calc
    addi $a0, $a0, 1
str_to_float.calc:
    
    lbu  $t0, 0($a0)
    beq  $t0, $zero, str_to_float.done
    li   $t1, '.'
    bne  $t0, $t1, str_to_float.not_point
    li   $t2, 1
    addi $a0, $a0, 1
    j    str_to_float.calc
str_to_float.not_point:
    li   $t1, '0'
    bltu $t0, $t1, str_to_float.error
    li   $t1, '9'
    bltu $t1, $t0, str_to_float.error
    subi $t0, $t0, '0'
    beq  $t2, $zero, str_to_float.other
    div.d  $f4, $f4, $f8
str_to_float.other:
    mul.d  $f2, $f2, $f8
    mtc1   $t0, $f6
    cvt.d.w  $f6, $f6
    add.d  $f2, $f2, $f6
    addi $a0, $a0, 1
    j    str_to_float.calc
str_to_float.done:
    mul.d  $f2, $f2, $f4
    j    str_to_float.exit
str_to_float.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    li   $v0, 17  # exit
    syscall
    
str_to_float.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $ra, 12($sp)
    addi $sp, $sp, 16
    jr $ra

################################################################################
# description:
# (void* <v1>) read_operator(char* <s0>)
# read operator (char* <s0>) and store to address (void* <v1>)
# args:
#   s0: operator pointer
# returns:
#   v0: 1, witch type is symbol
#   v1: result address
#   v2: symbol length
################################################################################
read_operator:
    addi $sp, $sp, -36
    sw   $ra, 32($sp)
    sw   $s3, 28($sp)
    sw   $s2, 24($sp)
    sw   $s1, 20($sp)
    sw   $t4, 16($sp)
    sw   $t3, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    li   $t1, operator_size
    move $a0, $t1
    li   $v0, 9
    syscall
    move $t0, $v0
    move $t3, $t0
    addi $t1, $t1, -1
read_operator.loop:
    jal  is_delimiter
    lbu  $t4, 0($s0)
    beq  $t4, 0, read_operator.error
    bne  $v0, 0, read_operator.done
    sb   $t4, 0($t0)
    addi $s0, $s0, 1
    addi $t0, $t0, 1
    addi $t1, $t1, -1
    bnez $t1, read_operator.loop
read_operator.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
read_operator.done:
    sb   $zero, 0($t0)
    li   $s1, 1
    move $s2, $t3
    sub  $s3, $t0, $t3
    li   $a0, atom_size
    li   $v0, 9
    syscall
    sw   $s1, 0($v0)
    sw   $s2, 4($v0)
    sw   $s3, 8($v0)
    
read_operator.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $t3, 12($sp)
    lw   $t4, 16($sp)
    lw   $s1, 20($sp)
    lw   $s2, 24($sp)
    lw   $s3, 28($sp)
    lw   $ra, 32($sp)
    addi $sp, $sp, 36
    jr $ra

################################################################################
# description:
# is_delimiter()
################################################################################
is_delimiter:
    addi $sp, $sp, -12
    sw   $ra, 8($sp)
    sw   $t4, 4($sp)
    sw   $t2, 0($sp)
    
    lbu  $t4, 0($s0)
    beq  $t4, 0, is_delimiter.true
    li   $t2, ' '
    beq  $t4, $t2, is_delimiter.true
    li   $t2, '\t'
    beq  $t4, $t2, is_delimiter.true
    li   $t2, '\r'
    beq  $t4, $t2, is_delimiter.true
    li   $t2, '\n'
    beq  $t4, $t2, is_delimiter.true
    li   $t2, '('
    beq  $t4, $t2, is_delimiter.true
    li   $t2, ')'
    beq  $t4, $t2, is_delimiter.true
    li   $t2, '"'
    beq  $t4, $t2, is_delimiter.true
    li   $t2, ';'
    beq  $t4, $t2, is_delimiter.true
    li   $v0, 0
    j    is_delimiter.exit
is_delimiter.true:
    li   $v0, 1
    
is_delimiter.exit:
    lw   $t2, 0($sp)
    lw   $t4, 4($sp)
    lw   $ra, 8($sp)
    addi $sp, $sp, 12
    jr $ra


################################################################################
# description:
# (void* <v0>) read_list(char* <s0>)
# read list (char* <s0>) and store to address (void* <v0>)
# args:
#   s0: list pointer
# returns:
#   v0: result address
################################################################################
read_list:
    addi $sp, $sp, -32
    sw   $ra, 28($sp)
    sw   $s3, 24($sp)
    sw   $s2, 20($sp)
    sw   $s1, 16($sp)
    sw   $t3, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    jal  eat_whitespace
    jal  token
    move $t0, $s4
    li   $t1, type_token
    bne  $t0, $t1, read_list.error
    move $t0, $s5
    li   $t1, '('
    bne  $t0, $t1, read_list.error
    jal  is_next_rparen
    beq  $v0, $zero, read_list.not_nil
    addi $s0, $s0, 1
    move $v0, $s6
    j    read_list.exit
read_list.not_nil:
    jal  read_datum
    move $a0, $v0
    jal  single_list
    move $t2, $v0
    move $t3, $v0
read_list.loop:
    jal  eat_whitespace
    lbu  $t0, 0($s0)
    li   $t1, ')'
    beq  $t0, $t1, read_list.tail
    li   $t1, '.'
    beq  $t0, $t1, read_list.tail
    beq  $t0, $zero, read_list.error
    jal  read_datum
    move $a0, $v0
    jal  single_list
    sw   $v0, 8($t3)
    move $t3, $v0
    j    read_list.loop
read_list.tail:
    lbu  $t0, 0($s0)
    li   $t1, '.'
    bne  $t0, $t1, read_list.rparen
    addi $s0, $s0, 1
    jal  read_datum
    sw   $v0, 8($t3)
read_list.rparen:
    jal  token
    move $t0, $s4
    li   $t1, type_token
    bne  $t0, $t1, read_list.error
    move $t0, $s5
    li   $t1, ')'
    bne  $t0, $t1, read_list.error
    li   $s1, type_pair
    move $s2, $t2
    move $s3, $s6
    j    read_list.done
read_list.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    j    exit
    
read_list.done:
    li   $a0, pair_size
    li   $v0, 9
    syscall
    move $t0, $v0
    sw   $s1, 0($t0)
    sw   $s2, 4($t0)
    sw   $s3, 8($t0)
    move $v0, $t0
    
read_list.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $t3, 12($sp)
    lw   $s1, 16($sp)
    lw   $s2, 20($sp)
    lw   $s3, 24($sp)
    lw   $ra, 28($sp)
    addi $sp, $sp, 32
    jr $ra
    
################################################################################
# description:
# (void* <v0>) read_datum(char* <s0>)
# read datum (char* <s0>) and store to address (void* <v0>)
# args:
#   s0: datum pointer
# returns:
#   v0: result address
################################################################################
read_datum:
    addi $sp, $sp, -12
    sw   $ra, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    jal  eat_whitespace
    lbu  $t0, 0($s0)
    li   $t1, '('
    bne  $t0, $t1, read_datum.atom
    jal  read_list
    j    read_datum.exit
read_datum.atom:
    li   $a0, atom_size
    li   $v0, 9
    syscall
    move $t0, $v0
    jal  token
    lw   $s1, 0($v0)
    lw   $s2, 4($v0)
    lw   $s3, 8($v0)
    sw   $s1, 0($t0)
    sw   $s2, 4($t0)
    sw   $s3, 8($t0)
    move $v0, $t0
    
read_datum.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $ra, 8($sp)
    addi $sp, $sp, 12
    jr $ra



################################################################################
# description:
# is_next_rparen()
################################################################################
is_next_rparen:
    addi $sp, $sp, -16
    sw   $ra, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    move $t2, $s0
    jal  eat_whitespace
    lbu  $t0, 0($s0)
    li   $t1, ')'
    bne  $t0, $t1, is_next_rparen.false
    li   $v0, 1
    j    is_next_rparen.exit
is_next_rparen.false:
    move $v0, $zero
is_next_rparen.exit:
    move $s0, $t2
    
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $ra, 12($sp)
    addi $sp, $sp, 16
    jr $ra

################################################################################
# description:
# (void* <v0>) single_list(void* <a0>)
# create single list with item (void* <a0>) and store to address (void* <v0>)
# args:
#   a0: single list item
# returns:
#   v0: result address
################################################################################
single_list:
    addi $sp, $sp, -8
    sw   $ra, 4($sp)
    sw   $t0, 0($sp)
    
    move $t0, $a0
    li   $a0, pair_size
    li   $v0, 9
    syscall
    sw   $t0, 4($v0)
    li   $t0, type_pair
    sw   $t0, 0($v0)
    move $t0, $s6
    sw   $t0, 8($v0)

    lw   $t0, 0($sp)
    lw   $ra, 4($sp)
    addi $sp, $sp, 8
    jr $ra



################################################################################
# description:
# print_sexp()
# s-expression storage:
# 1. nil
#              -1    |    nil    |    nil
# 2. pair
#           0x0100   |    car    |    cdr
# 3. atom
# 3.1 string
#           0x0000   | str addr  | str length
# 3.2 symbol
#           0x0001   | sym addr  | sym length
# 3.3 boolean
#           0x0002   | bool val  | 
# 3.4 integer
#           0x0003   | int  val  | 1
# 3.5 fraction
#           0x0004   | numer val | denom val 
# 3.6 decimal
#           0x0005   | double val| double val 
# 3.7 character
#           0x0006   | int  val  | 
# 3.8 vector
#           0x0007   | vec addr  | vec length
################################################################################
print_sexp:
    addi $sp, $sp, -20
    sw   $ra, 16($sp)
    sw   $t3, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    lw   $t0, 0($a0)
    lw   $t1, 4($a0)
    lw   $t2, 8($a0)
    li   $t3, type_nil
    bne  $t0, $t3, print_sexp.pair
    la   $a0, nil_string
    li   $v0, 4
    syscall
    j    print_sexp.exit
print_sexp.pair:
    li   $t3, type_pair
    bne  $t0, $t3, print_sexp.string
    la   $a0, lparen_string
    li   $v0, 4
    syscall
    move $a0, $t1
    jal  print_sexp
    la   $a0, dot_string
    li   $v0, 4
    syscall
    move $a0, $t2
    jal  print_sexp
    la   $a0, rparen_string
    li   $v0, 4
    syscall
    j    print_sexp.exit
print_sexp.string:
    li   $t3, type_string
    bne  $t0, $t3, print_sexp.symbol
    li   $a0, '"'
    li   $v0, 11
    syscall
    move $a0, $t1
    li   $v0, 4
    syscall
    li   $a0, '"'
    li   $v0, 11
    syscall
    j    print_sexp.exit
print_sexp.symbol:
    li   $t3, type_symbol
    bne  $t0, $t3, print_sexp.boolean
    move $a0, $t1
    li   $v0, 4
    syscall
    j    print_sexp.exit
print_sexp.boolean:
    li   $t3, type_boolean
    bne  $t0, $t3, print_sexp.integer
    bne  $t1, $zero, print_sexp.true
    la   $a0, f_string
    li   $v0, 4
    syscall
    j    print_sexp.exit
print_sexp.true:
    la   $a0, t_string
    li   $v0, 4
    syscall
    j    print_sexp.exit
print_sexp.integer:
    li   $t3, type_integer
    bne  $t0, $t3, print_sexp.fraction
    move $a0, $t1
    li   $v0, 1
    syscall
    j    print_sexp.exit
print_sexp.fraction:
    li   $t3, type_fraction
    bne  $t0, $t3, print_sexp.decimal
    move $a0, $t1
    li   $v0, 1
    syscall
    li   $a0, '/'
    li   $v0, 11
    syscall
    move $a0, $t2
    li   $v0, 1
    syscall
    j    print_sexp.exit
print_sexp.decimal:
    li   $t3, type_decimal
    bne  $t0, $t3, print_sexp.character
    mtc1.d  $t1, $f12
    li   $v0, 3
    syscall
    j    print_sexp.exit
print_sexp.character:
    li   $t3, type_character
    bne  $t0, $t3, print_sexp.vector
    move $a0, $t1
    li   $v0, 11
    syscall
    j    print_sexp.exit
print_sexp.vector:
    li   $t3, type_vector
    bne  $t0, $t3, print_sexp.error
    li   $a0, '#'
    li   $v0, 11
    syscall
    la   $a0, lparen_string
    li   $v0, 4
    syscall
print_sexp.vector.loop:
    bleu $t2, $zero, print_sexp.vector.right
    move $a0, $t1
    jal  print_sexp
    addi $t2, $t2, -1
    j    print_sexp.vector.loop
print_sexp.vector.right:
    la   $a0, rparen_string
    li   $v0, 4
    syscall
    j    print_sexp.exit
print_sexp.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
print_sexp.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $t3, 12($sp)
    lw   $ra, 16($sp)
    addi $sp, $sp, 20
    jr $ra

################################################################################
# exit()
# 
################################################################################
exit:
    addi $sp, $sp, -16
    li   $v0, 17  # exit
    syscall

