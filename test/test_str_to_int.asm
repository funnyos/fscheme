################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
IPositive:     .asciiz   "123"
IZero:         .asciiz   "0"
INegtive:      .asciiz   "-123"
err_msg:       .asciiz   "Not a digit."
################################################################################
.text

.globl main

################################################################################
# main()
# entry point
################################################################################
main:
    la   $a0, IPositive
    jal  str_to_int
    move $a0, $v0
    li   $v0, 1
    syscall
    
    la   $a0, IZero
    jal  str_to_int
    move $a0, $v0
    li   $v0, 1
    syscall
    
    la   $a0, INegtive
    jal  str_to_int
    move $a0, $v0
    li   $v0, 1
    syscall
    
    li   $v0, 17  # exit
    syscall
################################################################################
# str_to_int()
# 
################################################################################
str_to_int:
    addi $sp, $sp, -16
    sw   $ra, 12($sp)
    sw   $t2, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)
    
    move $v0, $zero
    lbu  $t0, 0($a0)
    li   $t1, '-'
    bne  $t0, $t1, str_to_int.positive
    li   $t2, 1
    addi $a0, $a0, 1
    j    str_to_int.calc
str_to_int.positive:
    move $t2, $zero
    li   $t1, '+'
    bne  $t0, $t1, str_to_int.calc
    addi $a0, $a0, 1
str_to_int.calc:
    lbu  $t0, 0($a0)
    beq  $t0, $zero, str_to_int.done
    li   $t1, '0'
    bltu $t0, $t1, str_to_int.error
    li   $t1, '9'
    bltu $t1, $t0, str_to_int.error
    subi $t0, $t0, '0'
    mul  $v0, $v0, 10
    add  $v0, $v0, $t0
    addi $a0, $a0, 1
    j    str_to_int.calc
str_to_int.done:
    beq  $t2, $zero, str_to_int.exit
    mul  $v0, $v0, -1
    j    str_to_int.exit
str_to_int.error:
    la   $a0, err_msg
    li   $v0, 4
    syscall
    li   $v0, 17  # exit
    syscall
    
str_to_int.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $t2, 8($sp)
    lw   $ra, 12($sp)
    addi $sp, $sp, 16
    jr $ra

