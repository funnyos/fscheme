################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.eqv    buffer_size    256
.data
fin:           .asciiz "test_strlen.asm"      # filename for input
err_msg:       .asciiz "Error while reading file!\n"
################################################################################
.text

.globl main

################################################################################
# main()
# entry point
################################################################################
main:
    la   $a0, fin
    jal  read_file
    move $a0, $v0
    li   $v0, 4        # print_string
    syscall
    j    exit
################################################################################
# description:
# (char* <v0>) read_file(char* <a0>)
# read file with name (char* <a0>) to string (char* <v0>)
# args:
#   a0: file_name
# returns:
#   v0: file_content
################################################################################
read_file:
    addi $sp, $sp, -16
    sw   $ra, 12($sp)
    sw   $s0, 8($sp)
    sw   $t1, 4($sp)
    sw   $t0, 0($sp)

    li   $v0, 13       # system call for open file
    li   $a1, 0        # $a1=flags=O_RDONLY=0
    li   $a2, 0        # $a2=mode=0
    syscall            # open a file
    blt  $v0, 0, open_file.error
    move $s0, $v0      # store fd in $s0
    li   $a0, buffer_size
    li   $v0, 9
    syscall
    move $t1, $v0
    li   $v0, 14       # Syscall for Read from file
    move $a0, $s0      # Pass the file descriptor to argument a0
    move $a1, $t1      # Address of Buffer to read
    la   $a2, buffer_size
    syscall
read_file.loop:
    li   $a0, buffer_size
    li   $v0, 9
    syscall
    move $t0, $v0
    li   $v0, 14       # Syscall for Read from file
    move $a0, $s0      # Pass the file descriptor to argument a0
    move $a1, $t0      # Address of Buffer to read
    la   $a2, buffer_size
    syscall
    blt  $v0, 0, read_file.error
    beq  $v0, 0, read_file.done
    j read_file.loop	
read_file.done:
    li   $v0, 16       # 16=close file
    move $a0, $s0      # $s0 contains fd
    syscall            # close file
    move $v0, $t1
    j read_file.exit
open_file.error:
    li   $v0, 4
    la   $a0, err_msg
    syscall
    j read_file.exit
read_file.error:
    li   $v0, 16       # 16=close file
    move $a0, $s0      # $s0 contains fd
    syscall            # close file
    li   $v0, 4
    la   $a0, err_msg
    syscall
    j read_file.exit
	
read_file.exit:
    lw   $t0, 0($sp)
    lw   $t1, 4($sp)
    lw   $s0, 8($sp)
    lw   $ra, 12($sp)
    addi $sp, $sp, 16
    jr   $ra

################################################################################
# exit
################################################################################
exit:
    li   $v0, 17       # exit
    syscall







