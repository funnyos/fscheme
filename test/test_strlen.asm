################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
intro:         .asciiz   "Welcome to fscheme!\n"

################################################################################
.text

.globl main

################################################################################
# main()
# entry point
################################################################################
main:
    la   $a0, intro
    jal  strlen
    move $a0, $v0
    li   $v0, 01  # print_int
    syscall
    j    exit
    
################################################################################
# description:
# int <v0> strlen(char* <a0>)
# get length of zero-delimited string
# args:
#   a0: the string
# returns:
#   v0: its length
################################################################################
strlen:
    addi $sp, $sp, -8
    sw   $ra, 4($sp)
    sw   $t0, 0($sp)
    
    move  $v0, $zero                   # initialize counter
    lbu   $t0, ($a0)
    b     strlen.test
strlen.inc:
    lbu   $t0, ($a0)
    addi  $v0, $v0, 1
    addiu $a0, $a0, 1
strlen.test:
    bnez  $t0, strlen.inc
    addi  $v0, $v0, -1
    
    lw   $t0, 0($sp)
    lw   $ra, 4($sp)
    addi $sp, $sp, 8
    jr    $ra

################################################################################
# exit
################################################################################
exit:
    li   $v0, 17  # exit
    syscall
