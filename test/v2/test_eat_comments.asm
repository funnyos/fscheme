################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
intro:         .asciiz   " \t   \n  Welcome to fscheme!\n"
comment:       .asciiz   "   ;Welcome to fscheme!\n  Abc"

################################################################################
.text

.globl test_eat_comments

################################################################################
# test_eat_comments()
# entry point
################################################################################
test_eat_comments:
    la   $a0, intro
    jal  eat_comments
    move $a0, $v0
    li   $v0, 4  # print_string
    syscall
    la   $a0, comment
    jal  eat_comments
    move $a0, $v0
    li   $v0, 4  # print_string
    syscall
