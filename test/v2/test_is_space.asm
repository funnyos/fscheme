################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
prompt_input:             .asciiz   "\nInput:\n"
prompt_output:            .asciiz   "\nOutput:\n"
################################################################################
.text

.globl test_is_space

################################################################################
# test_is_space()
# entry point
################################################################################
test_is_space:
    addi $sp, $sp, -4
    sw   $ra, 0($sp)
	
	li   $a0, ' '
    jal  test_is_space_procedure
    li   $a0, '\t'
    jal  test_is_space_procedure
    li   $a0, '\r'
    jal  test_is_space_procedure
    li   $a0, '\n'
    jal  test_is_space_procedure
    li   $a0, '('
    jal  test_is_space_procedure
    li   $a0, ')'
    jal  test_is_space_procedure
    li   $a0, '"'
    jal  test_is_space_procedure
    li   $a0, ';'
    jal  test_is_space_procedure
	
	li   $a0, '0'
    jal  test_is_space_procedure
	li   $a0, '9'
    jal  test_is_space_procedure
	li   $a0, '\\'
    jal  test_is_space_procedure
	
	lw   $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

################################################################################
# test_is_space_procedure()
# entry point
################################################################################
test_is_space_procedure:
    addi $sp, $sp, -8
    sw   $ra, 4($sp)
    sw   $t0, 0($sp)
    
    move $t0, $a0

    la   $a0, prompt_input
    li   $v0, 4
    syscall
    
    move $a0, $t0
    li   $v0, 11
    syscall

    move $a0, $t0
    jal  is_space
    move $t0, $v0
    
    la   $a0, prompt_output
    li   $v0, 4
    syscall
    
    move $a0, $t0
    li   $v0, 1
    syscall
    
    lw   $t0, 0($sp)
    lw   $ra, 4($sp)
    addi $sp, $sp, 8
    jr $ra




