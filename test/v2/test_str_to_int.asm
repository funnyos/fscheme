################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
IPositive:     .asciiz   "123"
IZero:         .asciiz   "0"
INegtive:      .asciiz   "-123"
################################################################################
.text

.globl test_str_to_int

################################################################################
# test_str_to_int()
# entry point
################################################################################
test_str_to_int:
    la   $a0, IPositive
    jal  str_to_int
    move $a0, $v0
    li   $v0, 1
    syscall
    
    la   $a0, IZero
    jal  str_to_int
    move $a0, $v0
    li   $v0, 1
    syscall
    
    la   $a0, INegtive
    jal  str_to_int
    move $a0, $v0
    li   $v0, 1
    syscall
    
    li   $v0, 17  # exit
    syscall
