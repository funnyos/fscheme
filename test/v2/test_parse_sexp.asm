################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
sexp:             .asciiz "( a ( b \"1 2 3\") \"123\"@03 sdf334 )"
################################################################################
.text

.globl test_parse_sexp

################################################################################
# test_parse_sexp()
# entry point
################################################################################
test_parse_sexp:
    la   $a0, sexp
    jal  parse_sexp
    beq  $v1, $zero, test_parse_sexp.error
    move $a0, $v0
    jal  print_sexp
    li   $v0, 17  # exit
    syscall
test_parse_sexp.error:
    li   $v0, 17  # exit
    syscall
