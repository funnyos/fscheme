################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
FPositive:     .asciiz   "2/3"
FZero:         .asciiz   "0/1"
FNegtive:      .asciiz   "-4/3"
################################################################################
.text

.globl test_str_to_fraction

################################################################################
# test_str_to_fraction()
# entry point
################################################################################
test_str_to_fraction:
    la   $a0, FPositive
    jal  str_to_fraction
    move $a0, $v0
    move $a1, $v1
    #jal  print_fraction
    
    la   $a0, FZero
    jal  str_to_fraction
    move $a0, $v0
    move $a1, $v1
    #jal  print_fraction
    
    la   $a0, FNegtive
    jal  str_to_fraction
    move $a0, $v0
    move $a1, $v1
    #jal  print_fraction
    
    li   $v0, 17  # exit
    syscall
