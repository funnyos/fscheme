################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
title_error:              .asciiz   "[Tests for error]:\n"
title_digit_value:        .asciiz   "[Tests for digit_value]:\n"
title_hex_value:          .asciiz   "[Tests for hex_value]:\n"
title_is_delimiter:       .asciiz   "[Tests for is_delimiter]:\n"
title_is_space:           .asciiz   "[Tests for is_space]:\n"
################################################################################
.text

.globl main

################################################################################
# main()
# entry point
################################################################################
main:
    la   $a0, title_error
    li   $v0, 4
    syscall
    jal  test_error
	
	la   $a0, title_digit_value
    li   $v0, 4
    syscall
    jal  test_digit_value

    la   $a0, title_hex_value
    li   $v0, 4
    syscall
    jal  test_hex_value

    la   $a0, title_is_delimiter
    li   $v0, 4
    syscall
    jal  test_is_delimiter
	
	la   $a0, title_is_space
    li   $v0, 4
    syscall
    jal  test_is_space
	
	
    li   $v0, 17  # exit
    syscall
