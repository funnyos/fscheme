################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
prompt_input:             .asciiz   "\nInput:\n"
prompt_output:            .asciiz   "\nOutput:\n"
################################################################################
.text

.globl test_hex_value

################################################################################
# test_hex_value()
# entry point
################################################################################
test_hex_value:
    addi $sp, $sp, -4
    sw   $ra, 0($sp)
    
    li   $a0, '0'
    jal  test_hex_value_procedure
    
    li   $a0, '9'
    jal  test_hex_value_procedure
    
    li   $a0, 'a'
    jal  test_hex_value_procedure
    
    li   $a0, 'f'
    jal  test_hex_value_procedure
    
    li   $a0, 'A'
    jal  test_hex_value_procedure
    
    li   $a0, 'F'
    jal  test_hex_value_procedure
    
    li   $a0, 'x'
    jal  test_hex_value_procedure
    
    lw   $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra
################################################################################
# test_hex_value_procedure()
# entry point
################################################################################
test_hex_value_procedure:
    addi $sp, $sp, -8
    sw   $ra, 4($sp)
    sw   $t0, 0($sp)
    
    move $t0, $a0

    la   $a0, prompt_input
    li   $v0, 4
    syscall
    
    move $a0, $t0
    li   $v0, 11
    syscall

    move $a0, $t0
    jal  hex_value
    move $t0, $v0
    
    la   $a0, prompt_output
    li   $v0, 4
    syscall
    
    move $a0, $t0
    li   $v0, 1
    syscall
    
    lw   $t0, 0($sp)
    lw   $ra, 4($sp)
    addi $sp, $sp, 8
    jr $ra
