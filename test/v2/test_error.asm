################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
message1:      .asciiz   "this"
message2:      .asciiz   " is an"
message3:      .asciiz   " error"
message4:      .asciiz   " message."
################################################################################
.text

.globl test_error

################################################################################
# test_error()
# entry point
################################################################################
test_error:
    addi $sp, $sp, -4
    sw   $ra, 0($sp)
    
    li   $t6, -1
    la   $a0, message1
    jal  error
    
    li   $t6, -2
    la   $a0, message1
    la   $a1, message2
    jal  error
    
    li   $t6, -3
    la   $a0, message1
    la   $a1, message2
    la   $a2, message3
    jal  error
    
    li   $t6, -4
    la   $a0, message1
    la   $a1, message2
    la   $a2, message3
    la   $a3, message4
    jal  error
    
    lw   $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra
