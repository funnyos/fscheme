################################################################################
# fscheme (C) Copyright funnyos@qq.com 2018
#
# This software is freely distributable under the terms of the MIT 
# License. See the file LICENSE for details.
################################################################################
.data
DPositive:     .asciiz   "123.123"
DZero:         .asciiz   "0.0"
DNegtive:      .asciiz   "-123.123"
################################################################################
.text

.globl test_str_to_double

################################################################################
# test_str_to_double()
# entry point
################################################################################
test_str_to_double:
    la   $a0, DPositive
    jal  str_to_double
    mov.d  $f12, $f2
    li   $v0, 3
    syscall
    
    la   $a0, DZero
    jal  str_to_double
    mov.d  $f12, $f2
    li   $v0, 3
    syscall
    
    la   $a0, DNegtive
    jal  str_to_double
    mov.d  $f12, $f2
    li   $v0, 3
    syscall
    
    li   $v0, 17  # exit
    syscall
